const express = require('express');
const taskController = require('../controllers/taskController');
//Create a Router instance that functions as a middleware and routing system
//Allows access to HTTP method middlewares that makes it easier to create routes for our application
const router = express.Router();

//Route to get all the tasks
//http://localhost:3001/tasks/
router.get("/", (req, res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})


//Create a route for creating a task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(result => res.send(result));
})


//Route for deleting a task.
//http://localhost:3001/tasks/:id
//The colon (:) is an identifier that helps create a dynamic route which allows us to supply information in the URL
//The word that comes after the colon symbol will be the name of the URL parameter
//":id" is called WILDCARD where you can put any value, it then creates a link between "id" parameter in the URL and the value provided in the URL
router.delete("/:id", (req, res) => {
	//URL parameter values are accessed via the request object's "params" property
	//The property name of this object will match the given URL paramater name
	//In this case "id" is the name of the parameter
	//
	taskController.deleteTask(req.params.id).then(result => res.send(result));
})


//Update a task
router.patch("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(result => res.send(result))
})

router.get("/:id", (req, res) => {
	taskController.reviewTask(req.params.id).then(result => res.send(result))
})

router.put("/:id", (req, res) => {
	taskController.renewTask(req.params.id, req.body).then(result => res.send(result))
})

module.exports = router;
